//
//  NetworkServiceSignIn.swift
//  BreweriesTest
//
//  Created by user on 25.02.2019.
//  Copyright © 2019 Vlad Veretennikov. All rights reserved.
//

import Foundation

class NetworkServiceSignIn {
  private init() {}

  static func responseForSignIn(login: String, password: String, completion: @escaping(Int?) -> () ) {
    let userData = ["login": "\(login)", "password": "\(password)"]
    
    guard let url = URL(string: "https://www.reintodev.cz:4006/api/login?") else { return }
    
    var request = URLRequest(url: url)
    guard let httpBody = try? JSONSerialization.data(withJSONObject: userData, options: []) else { return }
    request.httpBody = httpBody
    request.httpMethod = "POST"
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    
    NetworkService.shared.logIn(urlRequest: request) { (response) in
      let dict = response as! [String : AnyObject]
      let userId = dict["userId"] as? Int
      completion(userId)
    }
  }
}
