//
//  ViewController.swift
//  BreweriesTest
//
//  Created by user on 24.02.2019.
//  Copyright © 2019 Vlad Veretennikov. All rights reserved.
//

import UIKit

class SignInVC: UIViewController {
  
  var userId = 0
  
  @IBOutlet weak var loginTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  
  @IBAction func signInButton(_ sender: UIButton) {
    NetworkServiceSignIn.responseForSignIn(login: loginTextField.text ?? "", password: passwordTextField.text ?? "") { (userId) in
      self.userId = userId ?? 0
      if self.userId != 0 {
        self.performSegue(withIdentifier: "SignIn", sender: nil)
      } else {
        let alert = UIAlertController(title: "Error", message: "You are not logged in, fill in all fields and log in", preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default) { (action) in return }
        alert.addAction(ok)
        self.present(alert, animated: true, completion: nil)
      }
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let dnvc = segue.destination as! UINavigationController
    let dvc = dnvc.topViewController as! BreweriesTableViewController
    dvc.userId = userId
  }
}

