//
//  GetAndSaveData.swift
//  BreweriesTest
//
//  Created by user on 27.02.2019.
//  Copyright © 2019 Vlad Veretennikov. All rights reserved.
//

import Foundation
import CoreData
import UIKit

final class StoreManager {
  
  static func deleteData() {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let fetchRequest: NSFetchRequest<Brewery> = Brewery.fetchRequest()
    
    do {
      let results = try context.fetch(fetchRequest)
      for managedObject in results {
        context.delete(managedObject)
      }
    } catch let error as NSError {
      print("Deleted all my data in Brewery error : \(error) \(error.userInfo)")
    }
  }
  
  static func breweries(completion: ([Brewery]) -> Void) {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let fetchRequest: NSFetchRequest<Brewery> = Brewery.fetchRequest()
    
    do {
      let breweries = try context.fetch(fetchRequest)
      completion(breweries)
    } catch let error {
      print(error)
    }
  }
  
  static func save(breweries: [StructBrewery]) {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    for dictionary in breweries {
      let entity = NSEntityDescription.entity(forEntityName: "Brewery", in: context)
      let brewery = NSManagedObject(entity: entity!, insertInto: context) as! Brewery
      brewery.id = Int64(dictionary.id)
      brewery.name = dictionary.name
      brewery.url = dictionary.url
      
      do {
        try context.save()
      } catch {
        print(error.localizedDescription)
      }
    }
  }
}
