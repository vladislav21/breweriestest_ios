//
//  Brewery+CoreDataProperties.swift
//  BreweriesTest
//
//  Created by user on 01.03.2019.
//  Copyright © 2019 Vlad Veretennikov. All rights reserved.
//
//

import Foundation
import CoreData


extension Brewery {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Brewery> {
        return NSFetchRequest<Brewery>(entityName: "Brewery")
    }

    @NSManaged public var id: Int64
    @NSManaged public var name: String
    @NSManaged public var url: String

}
