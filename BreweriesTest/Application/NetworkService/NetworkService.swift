//
//  NetworkService.swift
//  BreweriesTest
//
//  Created by user on 25.02.2019.
//  Copyright © 2019 Vlad Veretennikov. All rights reserved.
//

import Foundation

class NetworkService {
  
  private init() {}
  
  static let shared = NetworkService()
  
  func getData(url: URL, completion: @escaping (Any) -> ()) {
    let session = URLSession.shared
    
    session.dataTask(with: url) { (data, response, error) in
      guard let data = data else { return }
      DispatchQueue.main.async {
        completion(data)
      }
      }.resume()
  }
  
  func logIn(urlRequest: URLRequest, completion: @escaping (Any) -> ()) {
    let session = URLSession.shared
    
    session.dataTask(with: urlRequest) { (data, response, error) in
      guard let data = data else { return }
      
      do {
        let json = try JSONSerialization.jsonObject(with: data, options: [])
        DispatchQueue.main.async {
          completion(json)
        }
      } catch {
        print(error)
      }
      }.resume()
  }
}
