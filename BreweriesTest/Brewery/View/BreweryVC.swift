//
//  BreweryVC.swift
//  BreweriesTest
//
//  Created by user on 24.02.2019.
//  Copyright © 2019 Vlad Veretennikov. All rights reserved.
//

import UIKit
import WebKit

class BreweryVC: UIViewController {
  
  var brewery: Brewery?
  
  @IBOutlet weak var progressView: UIProgressView!
  @IBOutlet weak var breweryWebView: WKWebView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    title = brewery?.name
  }
  
  func settingWebView() {
    guard let breweryURL = brewery?.url else { return }
    guard let url = URL(string: breweryURL) else { return }
    let request = URLRequest(url: url)
    
    breweryWebView.load(request)
    breweryWebView.allowsBackForwardNavigationGestures = true
    breweryWebView.navigationDelegate = self
    breweryWebView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
  }
  
  override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    
    if keyPath == "estimatedProgress" {
      progressView.progress = Float(breweryWebView.estimatedProgress)
    }
  }
  
  private func showProgressView() {
    UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
      self.progressView.alpha = 1
    }, completion: nil)
  }
  
  private func hideProgressView() {
    UIView.animate(withDuration: 0.5, delay: 0, options: .curveEaseInOut, animations: {
      self.progressView.alpha = 0
    }, completion: nil)
  }
}

extension BreweryVC: WKNavigationDelegate {
  func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
    showProgressView()
  }
  
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    hideProgressView()
  }
  
  func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
    hideProgressView()
  }
}
