//
//  BreweriesTableViewCell.swift
//  BreweriesTest
//
//  Created by user on 24.02.2019.
//  Copyright © 2019 Vlad Veretennikov. All rights reserved.
//

import UIKit

class BreweriesTableViewCell: UITableViewCell {
  
  @IBOutlet weak var nameBreweryLabel: UILabel!
  @IBOutlet weak var goToBreweyButton: UIButton!
}
