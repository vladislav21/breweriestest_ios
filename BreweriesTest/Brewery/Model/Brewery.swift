//
//  Brewery.swift
//  BreweriesTest
//
//  Created by user on 24.02.2019.
//  Copyright © 2019 Vlad Veretennikov. All rights reserved.
//

import Foundation

struct StructBrewery: Codable {
  let id: Int
  let name: String
  let url: String
}
