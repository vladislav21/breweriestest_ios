//
//  NetworkServiceGetBrewery.swift
//  BreweriesTest
//
//  Created by user on 25.02.2019.
//  Copyright © 2019 Vlad Veretennikov. All rights reserved.
//

import Foundation

class NetworkServiceGetBrewery {
  
  static func getBrewery(userId: Int, completion: @escaping([StructBrewery]) -> ()) {
    let jsonUrlString = "https://www.reintodev.cz:4006/api/breweries?userId=\(userId)&skip=0"
    
    guard let url = URL(string: jsonUrlString) else { return }
    
    NetworkService.shared.getData(url: url) { (data) in
      do {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let breweries = try decoder.decode([StructBrewery].self, from: data as! Data)
        completion(breweries)
      } catch let error {
        print("Error serialization json", error)
      }
    }
  }
}
