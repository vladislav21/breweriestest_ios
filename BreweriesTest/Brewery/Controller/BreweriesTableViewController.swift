//
//  BreweriesTableViewController.swift
//  BreweriesTest
//
//  Created by user on 01.03.2019.
//  Copyright © 2019 Vlad Veretennikov. All rights reserved.
//

import UIKit

class BreweriesTableViewController: UIViewController {
  
  let activityIndicator = UIActivityIndicatorView()
  var breweries = [Brewery]()
  var userId = 0
  var index = 0
  
  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.rowHeight = 100
    initActivityIndicatorView()
    activityIndicator.startAnimating()
    NetworkServiceGetBrewery.getBrewery(userId: userId) { [weak self] (breweries) in
      StoreManager.deleteData()
      StoreManager.save(breweries: breweries)
      StoreManager.breweries(completion: { (breweries) in
        self?.breweries = breweries
        self?.activityIndicator.stopAnimating()
        self?.tableView.reloadData()
      })
    }
  }
  
  fileprivate func initActivityIndicatorView() {
    activityIndicator.center = tableView.center
    activityIndicator.hidesWhenStopped = true
    self.view.addSubview(activityIndicator)
  }

  // MARK: - Navigation
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let dvc = segue.destination as! BreweryVC
    dvc.brewery = breweries[index]
  }
  
  //MARK: - Actions
  
  @IBAction func brPressedButton(_ sender: UIButton) {
    index = sender.tag
    performSegue(withIdentifier: "ShowBrewery", sender: nil)
  }
}

//MARK: - Extensions

extension BreweriesTableViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return breweries.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! BreweriesTableViewCell
    cell.nameBreweryLabel.text = breweries[indexPath.row].name
    cell.goToBreweyButton.tag = indexPath.row
    
    return cell
  }
}
